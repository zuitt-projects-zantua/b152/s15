let user1 = {
	username: 'jackscepticeye',
	email: 'seanScepticeye@gmail.com',
	age: 31,
	topics: ['Gaming', 'Commentary']
}

function isLegalAge(user){
	if (user.age >= 18){
		console.log("Welcome to the Club.")
	}
	else {
		console.log("You cannot enter the club yet.")
	}
}

function addTopic(user,topic){
	if(topic.length >= 5){
		user.topics.push(topic);
	}
	else {
		console.log("Enter a topic with at least 5 characters.")
	}
}

console.log(user1);
isLegalAge(user1);

console.log(user1.topics)
addTopic(user1,"Comedy")
console.log(user1.topics)
addTopic(user1,"Math")
console.log(user1.topics)

let clubMember = ['jackscepticeye', 'pewdiepie'];

function register(member){
	if (member.length >= 8){
		clubMember.push(member)
	}
	else {
		console.log("Please enter a username longer or equal to 8 characters.")
	}
}

console.log(clubMember)
register("markiplier")
console.log(clubMember)
register("teejae")
console.log(clubMember)
