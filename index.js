console.log("Hello World from s15!");

let numString1 = "5";
let numString2 = "10";

let num1 = 4;
let num2 = 6;
let num3 = 1.5;
let num4 = .5;

//Operators
	//-allows our programming language to execute an operation or evaluation. In JS, when we use an operator a value is returned. For mathematical operators/operations, a number is returned. For comparison operators, a boolean is returned. These returned values can be saved in a variable which we can later use.

	//Mathematical Operators (+,-,/,*,%)
	//Addition Operator(+)
		console.log(num1+num2);//10
		//with the use of + (addition operator), we can return a value from addition between two number types. We can also save the returned value in a variable.

		let sum1 = num1+num2;
		console.log(sum1);//10

		let numString3 = numString1 + sum1;
		console.log(numString3);//510. It results to concatenation because at least one of the operators is a string. Javascript automatically converted the number to string because it detected that at least 1 of the operand is a string.

		/*
			number + number = proper mathematical addition, results to sum.

			string + num = string, because instead, concatenation happens.
		*/

		let sampleStr = "Charles";
		console.log(sampleStr+num2);//Charles6

	//Subtraction Operator (-)
		//Allows us to subtract the operands and result to a difference.
		//It returns a number that we can save in a variable.

		let difference1 = num1 - num3;
		console.log(difference1);//2.5

		//For subtraction operator, the string is converted back to number and then subtracted.

		let difference2 = numString2 - num2;
		console.log(difference2);//4

		let sampleStr2 = "Joseph";
		let difference3 = sampleStr2 - num1;
		console.log(difference3);//NaN - not a number. Joseph was converted to a number and resulted to NaN, NaN - number will result to NaN.

		let difference4 = numString2 - numString1;
		console.log(difference4);//5. Both numeric strings were converted into numbers and then subtracted.

		function subtract(num1,num2){
			return num1-num2;
		}

		let difference5 = subtract(10,7);
		console.log(difference5);


	//Multiplication Operator(*)
		//Multiply both operands and return the product as value.
		//Convert any string to number before multiplying.

		let product1 = num1*num2;
		console.log(product1);//24

		let product2=numString1*numString2;
		console.log(product2);//50

		/*
			Much like subtraction, string multiplied with a string, we convert the strings first into numbers then multiply. It will result to proper multiplication.
		*/

	//Division Operator (/)
		//Divide the right operand from the left operand
			//leftOperand/rightOperand
		//Convert string into numbers then divide

		let quotient1 = num2/num4;
		console.log(quotient1);//12

		let quotient2 = numString2/numString1;
		console.log(quotient2);//2


		function multiply(num1,num2){
			return num1*num2;
		}
		let product3 = multiply(4,5);
		console.log(product3);

		function divide(num1,num2){
			return num1/num2;
		}
		let quotient3 = divide(20,5);
		console.log(quotient3);

		/*multiplying or dividing to 0*/
		console.log(product3*0);//0
		console.log(quotient3/0);//Infinity

		//Modulo Operator (%)
			//Modulo is the remainder of a division operator.
			//5/5=1=0
			console.log(5%5);//0
			//leftOperand%rightOperand = modulo or the remainder
			console.log(25%6);//1
			//25/6 = remainder is 1
			console.log(50%2);//0

		//Assignment Operators
			//Basic Assignment Operator (=)

				//It allows us to assign value to the left operand.
				//It also allow us to assign an initial value to variable.

				/*
					leftOperand=rightOperand
				*/

			let variable = "initial value";
			variable = "new value";
			console.log(variable);//new value

			let sample1 = "sample value";
			variable = sample1;
			console.log(variable);//sample value
			console.log(sample1);//sample value

			let sample2 = "new sample value";
			variable = sample2;
			console.log(variable);//new sample value
			console.log(sample2);//new sample value

			/*a constant's value cannot be updated or re-assigned.*/

			/*const pi = 3.1416;
			pi = 5000;
			console.log(pi);*/

			//Note: Do not add a basic assignment (=) operator directly into the return statement

			//Addition Assignment Operator (+=)
				//The result of the addition operation is re-assigned as the value of the left operand.

				let sum2 = 10;
				sum2 += 20;
				//10+20 = 30
				//sum2 = 30
				console.log(sum2);//30

				let sum3 = 5;
				sum3 +=5;
				console.log(sum3);//10

				//When using addition assignment operator keep in mind that the left operand should be a variable.
				/*console.log(5+=5);*/


				let sum11 = 1;
				console.log(sum11+=1);//2

				let sum4 = 30;
				sum4 += "Curry";
				console.log(sum4);//30Curry

				let sum5 = "50";
				sum5 += "50";
				console.log(sum5);//5050

				let fullName = "Wardell"
				let name1 = "Stephen"
				fullName +=name1;
				console.log(fullName);//WardellStephen

				fullName += "Curry";
				console.log(fullName);

			//Subtraction Assignment Operator (-=)
				//The result of subtraction between both operands will be saved/re-assigned as the value of the leftOperand

				let numSample = 50;
				numSample -= 10;
				console.log(numSample);//40


				let numberString = "100";
				let numberString2 = "50";
				numberString -= numberString2;
				console.log(numberString);//50

				let text = "ChickenDinner";
				text -= "Dinner";
				console.log(text);//NaN

			//Multiplication Assignment Operator (*=)
				//the result of multiplication between the operands will be re-assigned as the value of the left operand

				let sampleNum1 = 3;
				let sampleNum2 = 4;
				sampleNum1 *= sampleNum2;
				console.log(sampleNum1);//12
				console.log(sampleNum2);//4

				sampleNum2 *= sampleNum1;
				console.log(sampleNum2);//48 = 4 * 12

			//Division Assignment Operator (/=)
				//the result of division between both operands will be re-assigned as the value of the leftOperand.

				let sampleNum5 = 70;
				let sampleNum6 = 10;
				sampleNum5 /= sampleNum6;
				console.log(sampleNum5);//7

			//Order of Operations follow (MDAS)

				let mdasResult = 1 + 2 - 3 * 4 / 5;
				console.log(mdasResult);//.6

				let pemdasResult = 1 + (2-3) * (4/5);
				console.log(pemdasResult);//.2 or 1.9999996

			//Increment and Decrement
				//Incrementation and Decrementation is adding or subtracting 1 from a variable and then re-assigning the result to the variable.

				//There are 2 implementation of this: Pre-fix and Post-fix


				//Pre-fix:

				let z = 1;
				++z;
				console.log(z);//2 - the value of z was added with 1 and it is immediately returned. with pre-fix increment/decrement, the incremented/decremented value is returned at once.

				//Post-fix
				console.log(z++);//2
				console.log(z);//3
				//with post-fix incrementation, we add 1 to the value of the variable, however, the difference is that the previous value is returned first before the incremented value.

				z++;//incrementation
				console.log(z);//4 - incremented value

				//Prefix vs Postfix
				console.log(++z);//5 - incremented first then returned the incremented value
				console.log(z++);//5 - previous value is returned first before incrementation
				console.log(z);//6. The new value is now returned.

			//Decrementation

				console.log(--z);//5

				console.log(z--);//5

				console.log(z);//4

			//Can we increment or decrement raw number data?
				//no, incrementation/decrementation are used on variables that contain number types.

				/*console.log(5++); - left hand side error.*/

			//Can we increment or decrement a string?
				//yes

				let sampleNumString = "5";
				console.log(++sampleNumString);//6

	//Comparison Operators are used to compare the values of the left and right operands.
	//Comparison Operators return a boolean (true/false)

		//Loose Equality Operator
			//Loose equality operator evaluates if the operands have the same value.

			console.log(1 == 1);

			//we can also save the result of comparison in a variable.

			let isSame = 5 == 5;
			console.log(isSame);//true

			console.log(1 == '1');//true - loose equality operator prioritizes the sameness of the value because loose equality operator actually enforces what we call Forced Coercion or when the types of the operands are automatically changed before the comparison. The string here was actually converted to number. 1 == '1' = true

			console.log(0 == false);//true - with forced coercion, since the operands have different types, both were converted to number, 0 is already a number but false is a boolean, the boolean false when converted to a number is equal to 0. And thus, 0 == false = true.

			//JS has a function/method which allow us to convert data from one type to number.

			let sampleConvert = Number(false);
			console.log(sampleConvert);

			let sampleConvert2 = "2500";
			sampleConvert2 = Number(sampleConvert2);
			console.log(sampleConvert2);//2500 - changed to number type.

			console.log(1 == true);//true

			console.log(5 == "5");//true

			console.log(true == "true");//false - forced coercion wherein both operands are converted to numbers. boolean true was converted to a number as 1. string "true" was converted to a number but since it was a word/alphanumeric, it resulted to NaN
			//1 == NaN is false.


		//Strict Equality Operator
			//Strict Equality operator evaluates the sameness of both values and types of operands. Thus, Strict Equality Operator is more preferred because JS is a loose-typed language.

			console.log(1 == "1");//true

			console.log(1 === "1");//false - checks sameness of both values and types of the operands.

			console.log("james2000" === "James2000");//false

			console.log(55 == "55");//false - strict equality operator (===) checks sameness of value AND type.

		//Loose inequality operator
			//checks whether the operands are not equal and/or have different values.
			//Much like Loose Equality Operator, Loose inequality operator also does forced coercion.

			console.log(1 != "1");//false
			//forced coercion
			//both operands converted to number:
			//string "1" is also converted to number.
			//1 != 1 = equal so: not inequal = false.

			/*Loose inequality Operator returns true if the operands are NOT equal, it will false if the operands are found to be equal.*/

			console.log("James" != "John");//true

		//Strict Inequality Operator
			//Strict Inequality Operator will check whether the two operands have inequal type or value.

			console.log(5 !==5);//false - operands have equal value and same type.

			console.log(5 !== "5");//true - operands have same value they have different types.

			console.log(true !== "true");//true - operands are inequal because they have different types.

		//Equality Operators and Inequality Operators with Variables:

		let nameStr1 = "Juan";
		let nameStr2 = "Jack";
		let numberSample = 50;
		let numberSample2 = 60;
		let numStr1 = "15";
		let numStr2 = "25";

		console.log(numStr1 == 50);//false
		console.log(numStr1 == 15);//true
		console.log(numStr2 === 25);//false
		console.log(nameStr1 != "James");//true
		console.log(numberSample !== "50");//true - because different type
		console.log(numberSample != "50");//false
		console.log(nameStr1 == nameStr2);//false
		console.log(nameStr2 === "jack");//false

	//Relational Comparison Operators
		//A comparison operator which will check the relationship between operands and return boolean

		let price1 = 500;
		let price2 = 700;
		let price3 = 8000;
		let numStrSample = "5500";

		//greater than (>)
		console.log(price1 > price2);//false
		console.log(price3 > price2);//true
		console.log(price3 > numStrSample);//true - forced coercion

		//less than
		console.log(price2 < price3);//true
		console.log(price1 < price3);//true
		console.log(price3 < 1000);//false
		console.log(numStrSample < price1);//false - forced coercion

		//Greater than or equal to
		console.log(price1 >= 500);//true
		console.log(price3 >= 10000);//false
		console.log(price2 >= 600);//true

		//Less than or equal to
		console.log(price2 <= numStrSample);//true
		console.log(price3 <= price1);//false
		console.log(price2 <= 700);//true

	//Logical Operators
		//and &&
			//And operator evaluates both the left and right operands and both left and right operands must be true so that the operation would result to true. If atleast one operand is false then, the operation would result to false.

			let isAdmin = false;
			let isRegistered = true;
			let isLegalAge = true;

			console.log(isRegistered && isLegalAge);//true
			console.log(isAdmin && isRegistered);//false

			let user1 = {
				username: "peterphoenix_1999",
				age: 28,
				level: 15,
				isGuildAdmin: false,
			}

			let user2 = {
				username: "kingBrodie00",
				age: 13,
				level: 50,
				isGuildAdmin: true,
			}

			//to be able to access the properties of an object we use dot notation(.):
			console.log(user1.username);//peterphoenix1999
			console.log(user1.level);//15

			//Authorization
			let authorization1 = user1.age >= 18 && user1.level >=25;
			console.log(authorization1);//false

			let authorization2 = user2.age >= 18 && user2.level >=25;
			console.log(authorization2);//false

			//Guild Leaders / Admin Meeting
			let authorization3 = user1.level >=10 && user1.isGuildAdmin === true;
			console.log(authorization3);//false

			let authorization4 = user2.level >=10 && user2.isGuildAdmin === true;
			console.log(authorization4);//true

		//or ||
			//Or Operator returns true if atleast one operand results to true

			/*
			let user1 = {
				username: "peterphoenix_1999",
				age: 28,
				level: 15,
				isGuildAdmin: false,
			}

			let user2 = {
				username: "kingBrodie00",
				age: 13,
				level: 50,
				isGuildAdmin: true,
			}
			*/

			//On-site meeting between new users
			let authorization5 = user1.age >=18 || user1.level <=15;
			console.log(authorization5);//true

			//Zoom meeting between users
			let authorization6 = user2.age >=18 || user2.level >= 1;
			console.log(authorization6);//true
			
			//Joining a new group with low levels
			let authorization7 = user1.level >= 10 || user1.isGuildAdmin === false;
			console.log(authorization7);//true

			//Joining a new group
			let authorization8 = user1.level >= 50 || user1.isGuildAdmin === false;
			console.log(authorization8);//true
			
		//not operator (!)
			//turns a boolean into the opposite value

			/*
			let isAdmin = false;
			let isRegistered = true;
			let isLegalAge = true;
			*/

			console.log(!isRegistered);//false
			console.log(!isAdmin);//true

	//Conditional Statements
		//A conditional statement is a key feature of a programming language
		//It allows us to perform certain tasks based on a condition
		//Sample Conditional Statements:
			//Is it windy today?(what do we do?)
			//Is it Monday today?(what do we do?)

	//If-Else Statements
		//If statement will run a code block if the condition specified is true or results to true

	/*
	let user1 = {
		username: "peterphoenix_1999",
		age: 28,
		level: 15,
		isGuildAdmin: false,
	}

	let user2 = {
		username: "kingBrodie00",
		age: 13,
		level: 50,
		isGuildAdmin: true,
	}
	*/

	if(user1.age >= 18){
		alert("You are allowed to enter!");
	}

	/*
		the code block of an If condition is run only when the condition given is met or results to true.

		syntax:

		if(true){
			//code block to run
		}

		else statement will be run if the given condition in the if statement is not met.

		if(true){
			//code block to run
		} else{
			//code block to run if If condition is not met
		}
	*/

	if(user2.age >= 18){
		alert("User2 you are allowed to enter.");
	} else {
		alert("User2, you are not allowed to enter.");
	}

	if(user1.level >= 20){
		console.log("User1 is not a noob.");
	} else {
		console.log("User1 is a noob.");
	}

	if(user2.level >= 20){
		console.log("User2 is not a noob.");
	} else {
		console.log("User2 is a noob.");
	}


	if(user1.isGuildAdmin === true){
		console.log("Welcome Back, Guild Admin.");
	} else{
		console.log("You are not authorized to enter.");
	}

	/*
		else-if

		-executes a code block if the previous/original condition is not met but we have met another specified condition

		-IF a statement has both else-if and else, else will run if all conditions in the if or else-if are not met.
	*/

	if(user2.level >= 35){
		console.log("Hello, Knight!");
	} else if(user2.level >= 25){
		console.log("Hello, Swordsman!");
	} else if(user2.level >= 10){
		console.log("Hello, Rookie!");
	} else {
		console.log("Level out of range");
	}

	//Logical Operators for if Conditions:

	let usernameInput1 = "nicolIsAwesome100";
	let passwordInput1 = "iamawesomenicole";

	let usernameInput2 = "";
	let passwordInput2 = null;

	

	function register(username,password){
		if(username === "" || password === null){
			console.log("please complete the form.");
		} else {
			console.log("Thank you for registering.");
		}	
	}

	register(usernameInput1,passwordInput1);//if
	register(usernameInput2,passwordInput1);//else

	function requirementChecker(level,isGuildAdmin){
		if(level <= 25 && isGuildAdmin === false){
			console.log("Welcome to the Newbies Guild");
		} else if(level > 25){
			console.log("you are too strong.");
		} else if(isGuildAdmin === true){
			console.log("you are a guild admin.")
		}
	}

	requirementChecker(user1.level,user1.isGuildAdmin);//if
	requirementChecker(user2.level,user2.isGuildAdmin);//elseif1

	let user3 = {
		username: "richieBillions",
		age: 20,
		level: 20,
		isGuildAdmin: true,
	}

	requirementChecker(user3.level,user3.isGuildAdmin);

	function addNum(num1,num2){
		//check if the numbers being passed are both number types.
		//typeof keyword returns a string which tells the type of data that follows it
		if(typeof num1 === "number" && typeof num2 === "number"){
			console.log("run only if both arguments passed are number type")
		} else {
			console.log("one or both of the arguments are not numbers.")
		}
	}

	addNum(5,10);//if
	addNum("6",20);//else

	//typeof - used for validating the data type of variables or data.
	//it returns a string after evaluating the data type of the data/variable that comes after it.

	let str = "sample";
	console.log(typeof str);//string

	console.log(typeof 75);//number
	console.log(typeof true);//boolean


	function dayChecker(day){

		//toLowerCase() - makes a string all small caps.

	/*	day = day.toLowerCase();
		console.log(day);

		if(day === "sunday"){
			console.log("Today is Sunday; Wear White.");
		} else if(day === "monday"){
			console.log("Today is Monday; Wear Blue.");
		} else if(day === "tuesday"){
			console.log("Today is Tuesday; Wear Green.");
		} else if(day === "wednesday"){
			console.log("Today is Wednesday; Wear Purple.");
		} else if(day === "thursday"){
			console.log("Today is Thursday; Wear Brown.");
		} else if(day === "friday"){
			console.log("Today is Friday; Wear Red.");
		} else if(day === "saturday"){
			console.log("Today is Saturday; Wear Pink.");
		}
	}*/

	/*
		Switch is a conditional statement which can be an alternative to else-if, if-else, where the data being checked or evaluated is of an expected input.

		Switch will compare you expression/condition to match with a case. Then the statement for that case will run.

		syntax:

			switch(expression/condition){
				case "value":
				statement;
				break;
			default:
				statement;
				break;
			}

	*/

		switch(day.toLowerCase()){
			case "sunday":
			console.log("Today is " + day + "; Wear White");
				break;
			case "monday":
			console.log("Today is " + day + "; Wear Blue");
				break;
			case "tuesday":
			console.log("Today is " + day + "; Wear Green");
				break;
			case "wednesday":
			console.log("Today is " + day + "; Wear Purple");
				break;
			case "thursday":
			console.log("Today is " + day + "; Wear Brown");
				break;
			case "friday":
			console.log("Today is " + day + "; Wear Red");
				break;
			case "saturday":
			console.log("Today is " + day + "; Wear Pink");
				break;
			default:
				console.log("Invalid Input. Enter a valid day of the week.");
		}
	}
	dayChecker("sunday");


	let member = "Eugene";

	//the break statement is used to terminate the execution of the switch after it found a match

	switch(member){
		case "Eugene":
			console.log("You're power level is 20000");
			break;
		case "Dennis":
			console.log("Your power level is 15000");
			break;
		case "Vincent":
			console.log("Your power level is 14500");
			break;
		case "Jeremiah":
			console.log("Your power level is 10000");
			break;
		case "Alfred":
			console.log("Your power level is 8000");
			break;
		default:
			console.log("Invalid Input. Add a Ghost Fighter member.")
	}

	function capitalCheck(country){
		switch(country){
			case "philippines":
				console.log("Manila");
				break;
			case "usa":
				console.log("Washington D.C.");
				break;
			case "japan":
				console.log("Tokyo");
				break;
			case "germany":
				console.log("Berlin");
				break;
			default:
				console.log("Input is out of range. Choose another country.")
		}
	}

	capitalCheck("Philippines");

	//Ternary Operator
		//Conditional statement as if-else. However it was introduced to be short hand way to write if-else.

		/*syntax
			condition ? if-statement : else-statement
		*/

		let superHero = "Batman";
		superHero === "Batman" ? console.log("You are rich.") : console.log("Bruce Wayne is richer than you.");

		//Ternary operators require an else statement
		/*superHero === "Superman" ? console.log("Hi, Clark!");*/
		
		//Ternary operators can implicityly return value even without the return keyword.

		let currentRobin = "Dick Grayson";

		let isFirstRobin = currentRobin === "Dick grayson" ? true : false;
		console.log(isFirstRobin);







